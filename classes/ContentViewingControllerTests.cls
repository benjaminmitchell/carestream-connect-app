@isTest
public class ContentViewingControllerTests {

    public static testMethod void getOnLoadInfoTest() {
        //Make user with a set SF language, no Apollo language
        //Create a custom setting (Language - so we have picklist values to grab)
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        User sysAdmin = new User(Alias = 'newUser1', Email='newuser1@testorg.com', 
                 EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                 LocaleSidKey='en_US', ProfileId = p.Id, CARE_GlobalID__c = null, Content_Language_for_Apollo__c =  null,
                 TimeZoneSidKey='America/Los_Angeles', UserName='newuser1Nfwwiv88vH@testorg.com');
        
        insert new ContentViewingFilters__c(Name = 'Testing Language Custom Setting', Field_Name__c = 'Language__c', Filter_Label__c = 'Testing Language Label', Multiselect__c = true);
        
        System.runAs(sysAdmin) {
            ContentViewingController.Data data = ContentViewingController.getOnLoadInfo();
        	System.debug(data);
            System.assert(data.preferredLanguage != null);
            System.assert(data.languageOptions != null);
            System.assert(data.filters[0].fieldName == 'Language__c');
        }
        
        //loadContent
        String payload = ContentViewingController.loadContent('{"fields":["Language__c","Apollo_Category__c","Id","CarestreamDSA__Mobile_App_Vanity_Name__c","FileType","Title","Description_Text__c","Description","ContentDocument.Id"],"sObjectType":"ContentVersion","whereClause":"(Language__c = \'English\') AND (Title LIKE \'% %\' OR Description LIKE \'% %\' OR Description_Text__c LIKE \'% %\' OR CarestreamDSA__Mobile_App_Vanity_Name__c LIKE \'% %\') AND isLatest = true","recordLimit":20,"pageSize":20,"pageNumber":1,"offset":20}');
    }
    
    public static testMethod void testContentTracking() {
        ContentVersion cv = new ContentVersion(Title = 'Testing Document',
                                               ContentURL='https://www.google.com'); 
        insert cv;
        ContentViewingController.contentViewed(cv.Id);
        
        Content_usage__c newUsageLog = [SELECT Id FROM Content_usage__c LIMIT 1];
        System.assert(newUsageLog != null);
    }
    
    public static testMethod void saveAppFiltersTest() {
        ContentViewingController.saveAppFilters('Testing App Filters');
        
        User currentUser = [SELECT Id, Connect_Persistent_Filter__c 
                                FROM User 
                                WHERE Id = :UserInfo.getUserId()];
        System.assert(currentUser.Connect_Persistent_Filter__c == 'Testing App Filters');
    }
}