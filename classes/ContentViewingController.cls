public class ContentViewingController {
    
    public class Data {
        @AuraEnabled
        public String preferredLanguage;
        @AuraEnabled
        public List<String> languageOptions;
        @AuraEnabled
        public String contentAppFilters;
        @AuraEnabled
        public String SFLanguage;
        @AuraEnabled
        public List<FilterInfo> filters;
    }
    
    public class FilterInfo {
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String fieldLabel;
        @AuraEnabled
        public Boolean multiSelect;
        @AuraEnabled
        public List<String> picklistOptions;
    }
    
    @AuraEnabled
    public static Data getOnLoadInfo() {
        Data onLoadData = new Data();
        
        //Get the user's preferred Apollo search language - if it is empty, fill it with their
        //set Salesforce language
        User currentUserInfo = [SELECT Id, toLabel(LanguageLocaleKey), 
                                Content_Language_for_Apollo__c, Connect_Persistent_Filter__c
                                FROM User 
                                WHERE Id = :UserInfo.getUserId()];
        
        onLoaddata.contentAppFilters = currentUserInfo.Connect_Persistent_Filter__c;
        onLoadData.SFLanguage = currentUserInfo.LanguageLocaleKey;
        
        if(currentUserInfo.Content_Language_for_Apollo__c == null) {
            String language = currentUserInfo.LanguageLocaleKey;
            saveNewPreferredLanguage(language);
            onLoadData.preferredLanguage = language;
        }
        else {
            onLoadData.preferredLanguage = (String)currentUserInfo.Content_Language_for_Apollo__c;
        }
        
        //Get all of the picklist options for the preferred Apollo language
        List<String> languageOptions = new List<String>();
        //languageOptions.add('Select...');
        List<Schema.PicklistEntry> ple = User.Content_Language_for_Apollo__c.getDescribe().getPicklistValues();
        for( Schema.PicklistEntry f : ple) { languageOptions.add(f.getValue()); }
        onLoadData.languageOptions = languageOptions;
        
        //Grab and build filters
        onLoadData.filters = getCustomSettingFilters();
        
        return onLoadData;
    }
    
    @AuraEnabled
    public static void saveNewPreferredLanguage(String lang) {
        System.debug('NEW LANG: ' + lang);
        
        User currentUser = [SELECT Id, Content_Language_for_Apollo__c 
                                FROM User 
                                WHERE Id = :UserInfo.getUserId()];
        
        currentUser.Content_Language_for_Apollo__c = lang;
        
        update currentUser;
    }
    
    @AuraEnabled
    public static void saveAppFilters(String filters) {
        System.debug(filters);
        
        User currentUser = [SELECT Id, Connect_Persistent_Filter__c 
                                FROM User 
                                WHERE Id = :UserInfo.getUserId()];
        
        currentUser.Connect_Persistent_Filter__c = filters;
        
        update currentUser;
    }
    
    @AuraEnabled
    public static List<FilterInfo> getCustomSettingFilters() {
        List<FilterInfo> filterInfo = new List<FilterInfo>();
        
        List<ContentViewingFilters__c> customSettingEntries = [SELECT Id, Field_Name__c, Filter_Label__c, Multiselect__c 
                                                               FROM ContentViewingFilters__c];
        
        for(ContentViewingFilters__c filter : customSettingEntries) {
            FilterInfo fi = new FilterInfo();
            fi.fieldName = filter.Field_Name__c;
            fi.fieldLabel = filter.Filter_Label__c;
            fi.multiSelect = filter.Multiselect__c;
            fi.picklistOptions = getPicklistValues(filter.Field_Name__c);
            
            filterInfo.add(fi);
        }
        
        return filterInfo;
    }
    
    @AuraEnabled
 	public static List<String> getPicklistValues(String fldName) {
        
  		List<String> options = new list<String>();
        
        //options.add('Select...');

        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get('ContentVersion').getDescribe().fields.getMap();
         
        list<Schema.PicklistEntry> values = fieldMap.get(fldName).getDescribe().getPickListValues();
         
        for(Schema.PicklistEntry a: values) {
            options.add(a.getValue());
        }
        
        //HARDCODED Rmeove categories from the category filter options list that don't have Content representing it.
        if(fldName == 'Apollo_Category__c') {
            List<String> finalOptions = new list<String>();
            for(String o : options) {
                List<ContentVersion> cvs = [SELECT Id, Apollo_Category__c FROM ContentVersion WHERE Apollo_Category__c = :o];
                //If there is at least one ContentVersion record within that category, add it to the finalOptions list
                if(cvs.size() > 0) {
                    finalOptions.add(o);
                }
            }
            return finalOptions;
        }
        
        return options;
 	}
    
    @AuraEnabled
    public static String loadContent(String payloadJSON) {
        system.debug(payloadJSON);
        RAL_Payload payload = (RAL_Payload)JSON.deserialize(payloadJSON, RAL_Payload.class);
        RAL_Query.execute(payload);
        Content_Usage_Log__c newLog = new Content_Usage_Log__c(Message__c = payload.soql);
        insert newLog;
        payloadJSON = JSON.serialize(payload);
        System.debug(payloadJSON);
        return payloadJSON;
    }
    
    @AuraEnabled
    public static void contentViewed(Id contentId) {
        User currentUser = [SELECT Id, Name, CARE_GlobalID__c, CARE_Region__c
                                FROM User 
                                WHERE Id = :UserInfo.getUserId()];
        
        ContentVersion viewedContent = [SELECT Id, CarestreamDSA__Mobile_App_Vanity_Name__c,
                                        Title, Language__c, Region__c, Vertical__c, ContentDocument.ParentId
                                        FROM ContentVersion
                                        WHERE Id = :contentId];
        
        List<ContentWorkspace> workspace = [SELECT Id, Name 
                                      FROM ContentWorkspace 
                                      WHERE Id = :viewedContent.ContentDocument.ParentId
                                      LIMIT 1];
        
        Content_usage__c newLog = new Content_Usage__c(
        	DocName__c = viewedContent.Title,
            Action__c = 'Viewed',
            Product__c = viewedContent.Vertical__c,
            Content_Language__c = viewedContent.Language__c,
            Region__c = currentUser.CARE_Region__c,
            When__c = System.now(),
            Who__c = currentUser.Name,
            Accessed_From_Connect__c = true
        );
        if(workspace.size() > 0) { newLog.Library__c = workspace[0].Name; }
        insert newLog;
    }
}