@isTest
public class RAL_Query_Tests {

    @isTest
    public static void test() {
        
        Account parentA = RAL_AccountTestData.default();
        insert parentA;
        
		Account a = RAL_AccountTestData.default();
        a.ParentId = parentA.Id;
        insert a;        
        
        RAL_Payload payload = new RAL_Payload();
        payload.sObjectType = 'Account';
        payload.fields = new List<String> { 'Id', 'Name', 'Parent.Name' };
		payload.orderByClause = 'Name desc';
		RAL_Query.execute(payload);
        system.assertEquals(2, payload.records.size());
        
    }
    
    @isTest
    public static void testSelectClause() {
        
        Account a = RAL_AccountTestData.default();
        insert a;
        
        RAL_Payload payload = new RAL_Payload();
        payload.selectClause = 'select Id, Name from Account';
		RAL_Query.execute(payload);
        system.assertEquals(1, payload.records.size());
        
    }
    
    @isTest
    public static void testSelectClauseAggregate() {
        
        Account a = RAL_AccountTestData.default();
        insert a;
        
        RAL_Payload payload = new RAL_Payload();
        payload.selectClause = 'select count(Name) from Account group by Id';
		RAL_Query.execute(payload);
        system.assertEquals(1, payload.records.size());
        
    }
    
    @isTest
    public static void testSelectClauseInvalidField() {
        
        Account a = RAL_AccountTestData.default();
        insert a;
        
        RAL_Payload payload = new RAL_Payload();
        payload.selectClause = 'select InvalidField from Account';
		RAL_Query.execute(payload);
        system.assertEquals(false, payload.success);
        
    }
    
    @isTest
    public static void testSelectClauseInvalidRelatedField() {
        
        Account a = RAL_AccountTestData.default();
        insert a;
        
        RAL_Payload payload = new RAL_Payload();
        payload.selectClause = 'select InvalidRelated.InvalidField from Account';
		RAL_Query.execute(payload);
        system.assertEquals(false, payload.success);
        
    }
    
    @isTest
    public static void testQueryWithJSON() {
        
        Account a = RAL_AccountTestData.default();
        insert a;
        
        RAL_Payload payload = new RAL_Payload();
        payload.selectClause = 'select Id, Name from Account';
		String payloadJSON = RAL_Query.query(JSON.serialize(payload));
        payload = (RAL_Payload)JSON.deserialize(payloadJSON, RAL_Payload.class);
        system.assertEquals(1, payload.records.size());
        
    }
    
}