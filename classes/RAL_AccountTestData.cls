public class RAL_AccountTestData {

    public static Account default() {
        Account a = new Account(Name = 'TEST');
        //Added Specifically for Carestream - they require this field upon account creation.
        a.BillingCountry = 'FR';
        return a;
    } 
    
}