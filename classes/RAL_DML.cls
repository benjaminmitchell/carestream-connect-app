public class RAL_DML {

    @AuraEnabled
    public static String saveRecords(String payloadJSON) {
        system.debug(payloadJSON);
		RAL_Payload payload = (RAL_Payload)JSON.deserializeStrict(payloadJSON, RAL_Payload.class);

        List<sObject> inserts = new List<sObject>();
        List<sObject> updates = new List<sObject>();
        List<String> actions = new List<String>();
        
        for(sObject r : payload.records) {
            if(r.Id == null) {
                inserts.add(r);
                actions.add('insert');
            }
            else {
                actions.add('update');
                updates.add(r);
            }
        }
        
        List<Database.SaveResult> insertResults = Database.insert(inserts, false);
        List<Database.SaveResult> updateResults = Database.update(updates, false);
        List<Database.SaveResult> allResults = new List<Database.SaveResult>();

        Boolean hasSaveResultErrors = false;
        Integer saveResultErrorCount = 0;
        Integer saveResultSuccessCount = 0;
        String[] saveResultErrorMessages = new String[] {};
        
        for(String s : actions) {
            if(s == 'insert') {
                if(insertResults[0].success == false) {
                    hasSaveResultErrors = true;
                    saveResultErrorCount++;
                    for(Database.Error e : insertResults[0].errors) {
                        saveResultErrorMessages.add(e.message);
                    }
                }
                else {
                    saveResultSuccessCount++;
                }
                allResults.add(insertResults[0]);
                insertResults.remove(0);
            }
            else {
                if(updateResults[0].success == false) {
                    hasSaveResultErrors = true;
                    saveResultErrorCount++;
                    for(Database.Error e : updateResults[0].errors) {
                        saveResultErrorMessages.add(e.message);
                    }
                }
                else {
                    saveResultSuccessCount++;
                }
                allResults.add(updateResults[0]);
                updateResults.remove(0);
            }
        }
  
        payload.saveResults = allResults;
        payload.hasSaveResultErrors = hasSaveResultErrors;
        payload.saveResultSuccessCount = saveResultSuccessCount;
        payload.saveResultErrorCount = saveResultErrorCount;
        payload.saveResultErrorMessages = saveResultErrorMessages;
        return JSON.serialize(payload);
        
    }
    
    @AuraEnabled
    public static String deleteRecords(String payloadJSON) {
        system.debug(payloadJSON);
		RAL_Payload payload = (RAL_Payload)JSON.deserializeStrict(payloadJSON, RAL_Payload.class);

        List<sObject> deletes = new List<sObject>();
        
        for(sObject r : payload.records) {
            deletes.add(r);
        }
        
        List<Database.DeleteResult> deleteResults = Database.delete(deletes, false);
        
        Boolean hasDeleteResultErrors = false;
        Integer deleteResultErrorCount = 0;
        Integer deleteResultSuccessCount = 0;
        String[] deleteResultErrorMessages = new String[] {};
        
        for(Database.DeleteResult r : deleteResults) {
            if(r.success == false) {
                hasDeleteResultErrors = true;
                deleteResultErrorCount++;
                for(Database.Error e : r.errors) {
                    deleteResultErrorMessages.add(e.message);
                } 
            }
            else {
                deleteResultSuccessCount++;
            }
        }
  
        payload.deleteResults = deleteResults;
        payload.hasDeleteResultErrors = hasDeleteResultErrors;
        payload.deleteResultSuccessCount = deleteResultSuccessCount;
        payload.deleteResultErrorCount = deleteResultErrorCount;
        payload.deleteResultErrorMessages = deleteResultErrorMessages;
        return JSON.serialize(payload);
        
    }

}