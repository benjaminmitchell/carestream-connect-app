public with sharing class RAL_Describe {
	
	public static Schema.DescribeFieldResult describeField(String objectName, String fieldOrPath) {
		Schema.DescribeSObjectResult sor = Schema.getGlobalDescribe().get(objectName).getDescribe();
		system.debug(fieldOrPath);
		if(fieldOrPath.indexOf('.') > -1) {
			// it's a path - need to figure out the associated lookup object
			String relName = fieldOrPath.split('\\.')[0];
			for(String s : sor.fields.getMap().keySet()) {
				Schema.DescribeFieldResult fr = sor.fields.getMap().get(s).getDescribe();
				if(fr.getRelationshipName() == relName) {
					return RAL_Describe.describeField(fr.getReferenceTo()[0].getDescribe().getName(), fieldOrPath.substringAfter('.'));
				}
			}
			return null;
		}
		else {
			// it's a field - go get it
			if(!sor.fields.getMap().containsKey(fieldOrPath)) return null;
			Schema.DescribeFieldResult fr = sor.fields.getMap().get(fieldOrPath).getDescribe();
			return fr;
		}
	}


}