global with sharing class RAL_Payload {

	// used by RAL_Query to execute SOQL calls - for overriding to write a fully custom query
	public String selectClause			{ get; set; }

	// used by RAL_Query to execute SOQL calls - builds fields list in "SELECT"
	public String[] fields				{ get { if(fields == null) { fields = new String[]{}; } return fields; } set; }

	// used by RAL_Query to execute SOQL calls - builds "FROM" statement
	public String sObjectType			{ get; set; }

	// used by RAL_Query to execute SOQL calls - builds "ORDER BY" statement
	public String orderByClause 		{ get { if(orderByClause == null) { orderByClause = ''; } return orderByClause; } set; }

	// used by RAL_Query to execute SOQL calls - builds "WHERE statement
	public String whereClause 			{ get; set; }

	// used by RAL_Query to execute SOQL calls - builds "NULLS" statement
	public String nullsLocation			{ get { if(nullsLocation == null) { nullsLocation = 'LAST'; } return nullsLocation; } set; }

	// used by RAL_Query to execute SOQL calls - maximum 10000
	public Integer recordLimit	 		{ get { if(recordLimit == null) { recordLimit = 10000; } return recordLimit; } set; }

	// used by RAL_Query to execute SOQL calls - returns query result records
    public sObject[] records 			{ get { if(records == null) { records = new sObject[] {}; } return records; } set; }
    
    // used by RAL_DML to report outcomes of saving records (insert or update)
    public Database.SaveResult[] saveResults	{ get { if(saveResults == null) { saveResults = new Database.SaveResult[] {}; } return saveResults; } set; }

	// used by RAL_DML to indicate that at least one error occurred when saving 
    public Boolean hasSaveResultErrors			{ get { if(hasSaveResultErrors == null) { hasSaveResultErrors = false; } return hasSaveResultErrors; } set; }

	// used by RAL_DML to indicate the number of errors that occurred when saving 
    public Integer saveResultErrorCount			{ get { if(saveResultErrorCount == null) { saveResultErrorCount = 0; } return saveResultErrorCount; } set; }

	// used by RAL_DML to indicate the number of errors that occurred when saving 
    public Integer saveResultSuccessCount			{ get { if(saveResultSuccessCount == null) { saveResultSuccessCount = 0; } return saveResultSuccessCount; } set; }
    
	// used by RAL_DML to indicate the number of errors that occurred when saving 
    public String[] saveResultErrorMessages		{ get { if(saveResultErrorMessages == null) { saveResultErrorMessages = new String[] {}; } return saveResultErrorMessages; } set; }
    
	// used by RAL_DML to report outcomes of saving records (insert or update)
    public Database.DeleteResult[] deleteResults	{ get { if(deleteResults == null) { deleteResults = new Database.DeleteResult[] {}; } return deleteResults; } set; }

	// used by RAL_DML to indicate that at least one error occurred when saving 
    public Boolean hasDeleteResultErrors			{ get { if(hasDeleteResultErrors == null) { hasDeleteResultErrors = false; } return hasDeleteResultErrors; } set; }

	// used by RAL_DML to indicate the number of errors that occurred when saving 
    public Integer deleteResultErrorCount			{ get { if(deleteResultErrorCount == null) { deleteResultErrorCount = 0; } return deleteResultErrorCount; } set; }

	// used by RAL_DML to indicate the number of errors that occurred when saving 
    public Integer deleteResultSuccessCount			{ get { if(deleteResultSuccessCount == null) { deleteResultSuccessCount = 0; } return deleteResultSuccessCount; } set; }
    
	// used by RAL_DML to indicate the number of errors that occurred when saving 
    public String[] deleteResultErrorMessages		{ get { if(deleteResultErrorMessages == null) { deleteResultErrorMessages = new String[] {}; } return deleteResultErrorMessages; } set; }

	// used by RAL_Query to execute SOQL calls - returns total count when set controller is in use
	public Integer totalRecords			{ get; set; }

	// used by RAL_Query to execute SOQL calls - returns boolean for next page when set controller is in use
	public Boolean hasNext				{ get; set; }

	// used by RAL_Query to execute SOQL calls - returns boolean for previous page when set controller is in use
	public Boolean hasPrevious			{ get; set; }

	// used by RAL_Query to execute SOQL calls to request and receive current page. default = 1
	public Integer pageNumber			{ get { if(pageNumber == null) { pageNumber = 1; } return pageNumber; } set; }

	// used by RAL_Query to execute SOQL calls to request and receive page size. default = 200
	public Integer pageSize				{ get { if(pageSize == null) { pageSize = 200; } return pageSize; } set; }

	// used by RAL_Query to return raw soql
	public String soql					{ get; set; }

	// used by RAL_Query to indicate that query was an aggregate query
	public Boolean isAggregate			{ get; set; }

	// used by RAL_Query to indicate that setcontroller was used
	public Boolean useSetController		{ get { if(useSetController == null) { useSetController = true; } return useSetController; } set; }

	// used by RAL_Query to indicate the outcome (success / failure) of a query
	public Boolean success { get; set; }

	// friendly message corresponding with success property
	public String message { get; set; }

	// system-level message from exception
	public String exceptionMessage { get; set; }	

	public ObjectDescribe objectDescribe { get; set; }
	public List<FieldDescribe> fieldDescribes { get; set; }

	public class ObjectDescribe {
		public String label { get; set; }
		public String name { get; set; }
	}

	public class FieldDescribe {
		public String label { get; set; }
		public String name { get; set; }
		public String type { get; set; }
		public Boolean required { get { if(required == null) { required = false; } return required; } set; }
		public List<String> picklistOptions { get; set; }
		public FieldDescribe() {
			picklistOptions = new List<String>();
		}
	}

}