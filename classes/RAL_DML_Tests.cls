@isTest
public class RAL_DML_Tests {

    @isTest
    public static void testInsert() {
        Account a = RAL_AccountTestData.default();
        RAL_Payload payload = new RAL_Payload();
        payload.records.add(a);
        RAL_DML.saveRecords(JSON.serialize(payload));
    }
    
    @isTest
    public static void testInsertFail() {
        Account a = RAL_AccountTestData.default();
        a.Name = null;
        RAL_Payload payload = new RAL_Payload();
        payload.records.add(a);
        RAL_DML.saveRecords(JSON.serialize(payload));
    }
    
    @isTest
    public static void testUpdate() {
        Account a = RAL_AccountTestData.default();
        insert a;
        a.Phone = '5555551212';
        RAL_Payload payload = new RAL_Payload();
        payload.records.add(a);
        RAL_DML.saveRecords(JSON.serialize(payload));
        RAL_DML.deleteRecords(JSON.serialize(payload));
    }
    
    @isTest
    public static void testUpdateFail() {
        Account a = RAL_AccountTestData.default();
        insert a;
        a.Name = null;
        a.Phone = '5555551212';
        RAL_Payload payload = new RAL_Payload();
        payload.records.add(a);
        RAL_DML.saveRecords(JSON.serialize(payload));
    }
    
}