global with sharing class RAL_Query {

	// aura-enabled alias
	@AuraEnabled
	global static String query(String payloadJSON) {
		return execute(payloadJSON);
	}

    global static String execute(String payloadJSON) {
        RAL_Payload payload = (RAL_Payload)JSON.deserialize(payloadJSON, RAL_Payload.class);
		payload = RAL_Query.execute(payload);
		payloadJSON = JSON.serialize(payload);
		return payloadJSON;
    }
    
	global static RAL_Payload execute(RAL_Payload payload) {
		
		try {
			
			if(!String.isEmpty(payload.selectClause)) {
				
				// reverse-engineer fields
				String tempSelectClause = payload.selectClause.toLowerCase().replace('  ', ' ');
				String tempFields = tempSelectClause.split(' from ')[0].replace('select ', '');
				tempFields = tempFields.replace(' ', '');
				payload.fields = tempFields.split(',');

				// reverse engineer sObjectType
				tempSelectClause = tempSelectClause.split(' from ')[1];
				payload.sObjectType = tempSelectClause.split(' ')[0];

			}

			else if(payload.fields != null && payload.sObjectType != null) {
				payload.selectClause = 'SELECT ' + String.join(payload.fields, ', ') + ' FROM ' + payload.sObjectType;
			}

			// CHECK OBJECT-LEVEL SECURITY
			payload.ObjectDescribe = new RAL_Payload.ObjectDescribe();

			Schema.DescribeSObjectResult od = Schema.getGlobalDescribe().get(payload.sObjectType).getDescribe();

			if(!od.isAccessible()) {
				payload.success = false;
				payload.message = payload.sObjectType + ' is not accessible by current user.';
				return payload;
			}
			else {
				payload.ObjectDescribe.label = od.getLabel();
				payload.ObjectDescribe.name = od.getName();
			}

			// CHECK FIELD-LEVEL SECURITY
			// FIELDS WILL BE REMOVED THAT ARE NOT ACCESSIBLE
			payload.fieldDescribes = new List<RAL_Payload.FieldDescribe>();

			List<String> tempFieldsList = new List<String>();

			for(String f : payload.fields) {
				Schema.DescribeFieldResult dfr = RAL_Describe.describeField(payload.sObjectType, f);
				if(dfr != null) {
					if(dfr.isAccessible()) {
						RAL_Payload.FieldDescribe d = new RAL_Payload.FieldDescribe();
						d.name = dfr.getName();
						d.label = dfr.getLabel();
						d.required = !dfr.isNillable();
						d.type = dfr.getType().name().toLowerCase();
						if(d.type == 'picklist' || d.type == 'multipicklist') {
							for(Schema.PicklistEntry  e : dfr.getPicklistValues()) {
								d.picklistOptions.add(e.getLabel());
							}
						}
						payload.fieldDescribes.add(d);	
						tempFieldsList.add(f);
					}
					
				}
			}

			payload.fields = tempFieldsList;

			payload.soql = payload.selectClause;

			payload.isAggregate = payload.soql.containsIgnoreCase(' group by ');
			
			if(!String.isEmpty(payload.whereClause)) payload.soql += ' WHERE ' + payload.whereClause;

			if(!String.isEmpty(payload.orderByClause)) {
				payload.soql += ' ORDER BY ' + payload.orderByClause;
				if(!String.isEmpty(payload.nullsLocation)) payload.soql += ' NULLS ' + payload.nullsLocation;
			}

			if(payload.recordLimit != null) payload.soql += ' LIMIT ' + String.valueOf(payload.recordLimit);
			
			system.debug(payload.soql);

			// because global variables are not supported...
			payload.soql = payload.soql.replace('$USERID', '\'' + UserInfo.getUserId() + '\'');
			
			Boolean isObjectListControllerCompatible = String.valueOf('Task|Attachment|Note|Document|CampaignMember|CampaignMemberStatus|AccountContactRelation|ContentVersion').indexOf(payload.sObjectType) == -1;
			
			if(payload.isAggregate || !isObjectListControllerCompatible || !payload.useSetController) {
                Integer offset = ((payload.pageNumber - 1) * payload.pageSize);
                if(offset > 0 && offset < 2001) { payload.soql += ' OFFSET ' + offset + ''; }
				payload.records = Database.query(payload.soql);
                
                //Finding the total number of results when the object is not supported in the standard setController
                String countSOQL = 'SELECT COUNT() FROM ' + payload.sObjectType;
                if(!String.isEmpty(payload.whereClause)) countSOQL += ' WHERE ' + payload.whereClause;
                payload.totalRecords = Database.countQuery(countSOQL);
                
                payload.success = true;
				return payload;
			}
			else {
                System.debug('The else');
				return executeSetController(payload, new ApexPages.StandardSetController(Database.getQueryLocator(payload.soql)));	
			}
		}
		catch(Exception ex) {
			payload.success = false;
			payload.message = 'An unexpected error occurred.';
			payload.exceptionMessage = ex.getMessage();
			return payload;
		}
	}
	
	private static RAL_Payload executeSetController(RAL_Payload payload, ApexPages.StandardSetController setController) {
		
        setController.setPageSize(payload.pageSize);
        setController.setPageNumber(payload.pageNumber);
        payload.records = setController.getRecords();
        payload.hasNext = setController.getHasNext();
        payload.hasPrevious = setController.getHasPrevious();
        payload.totalRecords = setController.getResultSize();
        payload.success = true;
        return payload;	
		
	}

}