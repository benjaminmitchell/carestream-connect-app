({
    init : function(component, event, helper) {
        var filter = component.get("v.filter");
        component.set("v.fieldName", filter.fieldName);
        component.set("v.label", filter.fieldLabel);
        component.set("v.multiSelect", filter.multiSelect);
        component.set("v.picklistOptions", filter.picklistOptions);
        component.set("v.values", []);
    },
    
	saveNewValues : function(component, event, helper) {
		//Save the newly selected items to the most recent values because "Apply" was chosen in the filterModal
		component.set("v.values", component.get("v.newValues"));
	},
    
    clearNewValues : function(component, event, helper) {
        //Reset the selected items back to previous state since "cancel" was chosen in the filterModal
        component.set("v.newValues", component.get("v.values"));
    },
    
    updateNewVals : function(component, event, helper) {
        var multiSelect = component.get("v.multiSelect");
        var selectedValue = component.get("v.selectedValue");
        var newValues = component.get("v.newValues");
        
        if(multiSelect === false || multiSelect === null) {
            var newList = [selectedValue];
            component.set("v.newValues", newList);
            return;
        }
        
        if(selectedValue === 'Select...' || selectedValue === null) { return; }
        
        if(newValues.length == 0) { newValues.push(selectedValue); }
        else {
            var present = false;
            for(var tag in newValues) {
                if(newValues[tag] === selectedValue) {
                    present = true;
                }
            }
            if(!present) { newValues.push(selectedValue); }
        }
        
        component.set("v.newValues", newValues);
        
        if(component.get('v.fieldName') == 'Apollo_Category__c') {
            //Trigger the parent's save function
            var parentRef = component.get("v.parentRef");
            parentRef.saveFilterModal();
            parentRef.set("v.search_text", "");
            parentRef.searchForContent();
        	//Trigger parent's search function with text of "" or " "
        }
    },
    
    removePill : function(component, event, helper) {
        //This is called from an aura:method, originating call from the ContentFilterPill component
        
        var params = event.getParam('arguments');
        if (params) {
            var pillName = params.pillName;
            var newValues = component.get("v.newValues");
            
            for(var tag in newValues) {
                if(newValues[tag] === pillName) {
                	newValues.splice(tag, 1);
                }
            }
            component.set("v.newValues", newValues);
        }
    }
})