({
	init : function(component, event, helper) {
		var content = component.get("v.content");
        
        //Title/Vanity Name
        var vanityName = content.CarestreamDSA__Mobile_App_Vanity_Name__c;
        if(vanityName !== null && vanityName !== undefined) {
            component.set("v.title", vanityName);
        }
        else {
            component.set("v.title", content.Title);
        }
        
        //Description
        var desc = content.Description_Text__c;
        if(desc !== null && desc !== undefined) {
            component.set("v.description", desc);
        }
        else {
            component.set("v.description", content.Description);
        }
        
        //File Type Icon
        component.set("v.fileIcon", helper.getFileIcon(component, event, helper));
        
        //Change date formatting
        helper.modifyDates(component, event, helper);
	},
    
    openSObject : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var content = component.get("v.content");
        var contentDocumentId = content.ContentDocumentId;
        
        navEvt.setParams({
            "recordId": contentDocumentId
        });
        navEvt.fire();
        
        helper.remoteCall(component, 'contentViewed', {
            contentId : content.Id
        }, false, function() {
        });
    }
})