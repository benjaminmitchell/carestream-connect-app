({
	remoteCall: function(cmp, call_name, params, cached, callback) {
        var action = cmp.get('c.' + call_name);
        action.setParams(params);
        if (cached) {
            action.setStorable();
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                callback(response.getReturnValue());
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getFileIcon : function(component, event, helper) {
        var fileIcon = component.get("v.content.FileType");
        var fileIconString = "doctype:";
        
        //console.log("File Icon: " + fileIcon);
        
        if(fileIcon === "PDF") { fileIconString += "pdf" ; }
        else if(fileIcon === "WORD" || fileIcon === "WORD_T" ||
                fileIcon === "WORD_X") { fileIconString += "word"; }
        else if(fileIcon === "POWER_POINT_X" || fileIcon === "POWER_POINT" ||
                fileIcon === "POWER_POINT_T" || fileIcon === "POWER_POINT_M") { fileIconString += "ppt"; }
        else if(fileIcon === "EXCEL_X" || fileIcon === "EXCEL" ||
                fileIcon === "EXCEL_M") { fileIconString += "excel"; }
        else if(fileIcon === "ZIP") { fileIconString += "zip"; }
        else if(fileIcon === "MP4") { fileIconString += "mp4"; }
        else if(fileIcon === "LINK") { fileIconString += "link"; }
        else if(fileIcon === "MP3" || fileIcon === "M4A") { fileIconString += "audio"; }
        else if(fileIcon === "EXE") { fileIconString += "exe"; }
        else if(fileIcon === "RTF") { fileIconString += "rtf"; }
        else if(fileIcon === "PACK") { fileIconString += "pack"; }
            else if(fileIcon === "JPG" || fileIcon === "JPEG" ||
                    fileIcon === "PNG") { fileIconString += "image"; }
        else if(fileIcon === "TEXT") { fileIconString += "txt"; }
        else if(fileIcon === "CSV") { fileIconString += "csv"; }
        else if(fileIcon === "VISIO") { fileIconString += "visio"; }
        else if(fileIcon === "HTM") { fileIconString += "xml"; }
        else if(fileIcon === "AVI" || fileIcon === "WMV" ||
               	fileIcon === "MOV") { fileIconString += "video"; }
        else if(fileIcon === "UNKNOWN") { fileIconString += "unknown"; }
        else { fileIconString += "attachment"; }
        
        //Canned - DWG, PPS
        
        //Obligatory sigh of relief
        //console.log("File Icon String: " + fileIconString);
        return fileIconString;
    },
    
    //Dates coming from SF need to be put into different format (mm/dd/yyyy)
    modifyDates : function(component, event, helper) {
        var lastModifiedDate = component.get("v.content.LastModifiedDate");
        
        var dateFormatter = function(dateStr) {
            console.log("Date Str: ");
            console.log(dateStr);
            var year = dateStr.substring(0, 4);
            var month = dateStr.substring(5, 7);
            var date = dateStr.substring(8, 10);
            
            var jsDate = new Date(year, month-1, date, 0, 0, 0, 0);

            console.log("JAVASCRIPT DATE: ");
            console.log(jsDate);
            var formattedMonth = jsDate.getMonth() + 1;
            var formattedDay = jsDate.getDate();
            return (formattedMonth + "/" + formattedDay + "/" + jsDate.getFullYear());
        }
        
        if(lastModifiedDate !== null && lastModifiedDate !== undefined) {
            component.set("v.content.LastModifiedDate", dateFormatter(lastModifiedDate));
        }
    }
})