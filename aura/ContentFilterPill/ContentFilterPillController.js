({
	removePill : function(component, event, helper) {
        var label = component.get("v.label");
        var FilterRef = component.get("v.FilterRef");
        FilterRef.removePill(label);
	}
})