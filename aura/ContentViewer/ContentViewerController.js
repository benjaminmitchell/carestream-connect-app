({
	init : function(component, event, helper) {
        helper.remoteCall(component, 'getOnLoadInfo', {}, false, function(data) {
          	console.log(data);
            component.set("v.preferredLanguage", data.preferredLanguage);
            component.set("v.preferredLanguageOptions", data.languageOptions);
            component.set("v.filters", data.filters);
            component.set("v.pageNumber", 1);
        	component.set("v.search_result", null);
            
            //Process the user's persistent filters
            if(data.contentAppFilters !== null && data.contentAppFilters !== undefined) {
                var appFilters = (JSON.parse(data.contentAppFilters).filters);
                for(var f in appFilters) {
                    var filter = appFilters[f];
                    
                    if(filter.fieldName === "fontSize") {
                    	component.set("v.fontSize", filter.values);
                        component.set("v.newFontSize", filter.values);
                    }
                    else if(filter.fieldName === "isLatest") {
                    	component.set("v.isLatest", filter.values);
                        component.set("v.newIsLatest", filter.values);
                    }
                    else {
                        var contentFilters = component.find("contentFilter");
                        for(var f in contentFilters) {
                            var fieldName = contentFilters[f].get("v.fieldName");
                            if(fieldName === filter.fieldName) {
                                contentFilters[f].set("v.values", filter.values);
                                contentFilters[f].set("v.newValues", filter.values);
                                
                                //Set the 'hint' filter pills on default load
                                var filterVals = contentFilters[f].get("v.values");
                                var currentVals = component.get("v.rawFilterVals");
                                var newRawFilterVals = filterVals.concat(currentVals);
                                component.set("v.rawFilterVals", newRawFilterVals);
                                //Break out of this for loop
                                break;
                            }
                        }
                    }
                }
            }
            else {
                //Defaults when the user doesn't have any pre-existing filter choices
            	component.set("v.isLatest", true);
        		component.set("v.newIsLatest", true);
                component.set("v.fontSize", "16");
        		component.set("v.newFontSize", "16");
                
                //Find the Language__c filter and tell it to set it's default language to the preferredLanguage
                var contentFilters = component.find("contentFilter");
                for(var f in contentFilters) {
                    var fieldName = contentFilters[f].get("v.fieldName");
                    if(fieldName === 'Language__c') {
                        contentFilters[f].set("v.values", contentFilters[f].get("v.preferredLanguage"));
                        contentFilters[f].set("v.newValues", contentFilters[f].get("v.preferredLanguage"));
                        
                        //Set the 'hint' filter pills on default load
                        var filterVals = contentFilters[f].get("v.values");
                        var currentVals = component.get("v.rawFilterVals");
                        var newRawFilterVals = filterVals.concat(currentVals);
                        component.set("v.rawFilterVals", newRawFilterVals);
                        //Break out of this for loop
                        break;
                	}
            	}
            }
        });
	},
    /*
    toggleLanguageModal : function(component, event, helper) {
        var modalOpen = component.get('v.languageModalOpen');
		modalOpen = !modalOpen;

		var languageModal = component.find('languageModal');

		if(modalOpen === true) {
			$A.util.removeClass(languageModal, 'slds-fade-in-hide');
        	$A.util.addClass(languageModal, 'slds-fade-in-open');
		}
		else {
			$A.util.addClass(languageModal, 'slds-fade-in-hide');
        	$A.util.removeClass(languageModal, 'slds-fade-in-open');
		}
        
		component.set('v.languageModalOpen', modalOpen);
    },
    
    closeLanguageModal : function(component, event, helper) {
        component.set("v.newPreferredLanguage", null);
        component.toggleLanguageModal();
    },
    
    saveLanguageModal : function(component, event, helper) {
        var newPreferredLanguage = component.get("v.newPreferredLanguage");
        if(newPreferredLanguage === "Select..." || newPreferredLanguage === null) {
            newPreferredLanguage = "English";
        }
        component.set("v.preferredLanguage", newPreferredLanguage);
        
		helper.remoteCall(component, 'saveNewPreferredLanguage', {
            lang : newPreferredLanguage
        }, false, function() {
          	component.set("v.newPreferredLanguage", null);
        });
        
        component.toggleLanguageModal();
    },*/
    
    toggleFilterModal : function(component, event, helper) {
        var modalOpen = component.get('v.filterModalOpen');
		modalOpen = !modalOpen;

		var filterModal = component.find('filterModal');
        var body = component.find("cmpBody");

		if(modalOpen === true) {
            //Main Body of the Component toggled (Hiding)
            $A.util.removeClass(body, 'slds-show');
            $A.util.addClass(body, 'slds-hide');
            
			$A.util.removeClass(filterModal, 'slds-fade-in-hide');
        	$A.util.addClass(filterModal, 'slds-fade-in-open');
		}
		else {
			$A.util.addClass(filterModal, 'slds-fade-in-hide');
        	$A.util.removeClass(filterModal, 'slds-fade-in-open');
            
            //Main Body of the component toggled (Opening)
            $A.util.removeClass(body, 'slds-hide');
        	$A.util.addClass(body, 'slds-show');
		}
        
		component.set('v.filterModalOpen', modalOpen);
    },
    
    closeFilterModal : function(component, event, helper) {
        //Loop through filters and clearNewValues
        var filters = component.find("contentFilter");
        for(var filter in filters) {
            filters[filter].clearNewValues();
        }
        component.set("v.newIsLatest", component.get("v.isLatest"));
        component.set("v.newFontSize", component.get("v.fontSize"));
        component.toggleFilterModal();
    },
    
    saveFilterModal : function(component, event, helper) {
        //Reset the rawFilterVals attribute
        component.set("v.rawFilterVals", null);
        
        //Loop through filters and saveNewValues
        var filters = component.find("contentFilter");
        for(var filter in filters) {
            filters[filter].saveNewValues();
            
            //Steal these vals and append to rawFilterVals
            var filterVals = filters[filter].get("v.values");
            var currentVals = component.get("v.rawFilterVals");
            var newRawFilterVals = filterVals.concat(currentVals);
            component.set("v.rawFilterVals", newRawFilterVals);
        }
        component.set("v.isLatest", component.get("v.newIsLatest"));
        component.set("v.fontSize", component.get("v.newFontSize"));
        component.searchForContent();
        helper.saveFilters(component, event, helper);
        component.toggleFilterModal();
    },
    
    searchForContent : function(component, event, helper) {
        //Code 13 = enter key, undefined = a button was pressed that targeted this function
        if (event.keyCode !== 13 && event.keyCode !== undefined) {
            //console.log(event.keyCode);
            return;
		}
        
        //Won't search when there's no search text
        var search_text = component.get("v.search_text");
        if(search_text === null || search_text === undefined) { 
            component.set("v.search_result", null);
            component.set("v.pageNumber", 1);
            return; 
        }
        
        //Will Refuse to search when the user hasn't selected a category in the filter settings
        if(helper.noCategorySelected(component, event, helper)) {
            component.set("v.search_result", null);
            component.set("v.pageNumber", 1);
            component.set("v.noCategorySelected", true);
            return;
        }
        else { component.set("v.noCategorySelected", false); }
        
        //Activate loading spinner
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
        
        //Fields to grab: Dynamic filter field additions, and hardcoded custom fields to display
        var fields = helper.getFilterFields(component, event, helper);
        fields.push("Id");
        fields.push("CarestreamDSA__Mobile_App_Vanity_Name__c");
        fields.push("FileType");
        fields.push("Title");
        fields.push("Description_Text__c");
        fields.push("Description");
        fields.push("ContentDocument.Id");
		fields.push("LastModifiedDate");
        fields.push("Content_Detail_Link__c");
        fields.push("Connect_Sequence__c");
        
        //Building the whereClause: Dynamic by chosen filters, and hardcoded search vs title/desc
        var whereClause = helper.buildWhereClause(component, event, helper);
        
        //Building the orderByClause
        var orderByClause = "Connect_Sequence__c ASC NULLS LAST, CarestreamDSA__Mobile_App_Vanity_Name__c ASC, Title ASC";

        //Hardcoded for content searching
        var sObjectType = "ContentVersion";
        
        var RECORD_LIMIT = 20;
        
        var payload = {
            fields : fields,
            sObjectType : sObjectType,
            whereClause : whereClause,
            orderByClause : orderByClause,
            recordLimit : RECORD_LIMIT,
            pageSize : RECORD_LIMIT,
            pageNumber : component.get("v.pageNumber"),
            offset : RECORD_LIMIT
        }; 
        var payloadJSON = JSON.stringify(payload);
        
        helper.remoteCall(component, 'loadContent', {
            payloadJSON : payloadJSON
        }, true, function(data) {
            data = JSON.parse(data);
          	console.log(data);
            if(data.records.length > 0) {
                component.set("v.search_result", data.records);
                
                component.set("v.totalResults", data.totalRecords);
                component.set("v.maxPageNum", Math.ceil((data.totalRecords / RECORD_LIMIT)));
                
                var pageNumber = component.get("v.pageNumber");
                var maxPageNum = component.get("v.maxPageNum");
                
                //Logic to disable each right arrow button
                if(pageNumber >= maxPageNum) {
                    component.find("plusOne").set("v.disabled", true);
                }
                if(pageNumber >= maxPageNum-4) {
                    component.find("plusMany").set("v.disabled", true);
                }
                
                //Logic to enable each right arrow button
                if(pageNumber < maxPageNum) {
                    component.find("plusOne").set("v.disabled", false);
                }
                if(pageNumber <= maxPageNum-5) {
                    component.find("plusMany").set("v.disabled", false);
                }
            }
            else {
                component.set("v.search_result", null);
            }
            
            //Turn off loading Spinner
            var spinner = component.find("spinner");
        	$A.util.addClass(spinner, "slds-hide");
        });
    },
    
    pagePlusOne : function(component, event, helper) {
        component.set("v.pageNumber", component.get("v.pageNumber") + 1);
        
        component.searchForContent();
    },
    
    pageMinusOne : function(component, event, helper) {
        component.set("v.pageNumber", component.get("v.pageNumber") - 1);
        
        component.searchForContent();
    },
    
    pagePlusMany : function(component, event, helper) {
        component.set("v.pageNumber", component.get("v.pageNumber") + 5);
        
        component.searchForContent();
    },
    
    pageMinusMany : function(component, event, helper) {
        var newPageNumber = component.get("v.pageNumber") - 5;
        if(newPageNumber < 1) { newPageNumber = 1; }
        component.set("v.pageNumber", newPageNumber);
        
        component.searchForContent();
    },
    
    resetPageNum : function(component, event, helper) {
        component.set("v.pageNumber", 1);
    },
    
    textSmall : function(component, event, helper) {
        component.set("v.newFontSize", "12");
    },
        
    textMedium : function(component, event, helper) {
        component.set("v.newFontSize", "16");
    },
        
    textLarge : function(component, event, helper) {
        component.set("v.newFontSize", "20");
    },
    
    checkFilterHints : function(component, event, helper) {
        var rawFilterVals = component.get("v.rawFilterVals");
        console.log(rawFilterVals);
        if(rawFilterVals !== null && rawFilterVals !== undefined 
           && rawFilterVals[0] !== null && rawFilterVals.length > 0) {
            component.set("v.showFilterHints", true);
        }
        else {
            component.set("v.showFilterHints", false);
        }
    }
})