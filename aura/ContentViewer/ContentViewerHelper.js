({
	remoteCall: function(cmp, call_name, params, cached, callback) {
        var action = cmp.get('c.' + call_name);
        action.setParams(params);
        if (cached) {
            action.setStorable();
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                callback(response.getReturnValue());
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getFilterFields : function(component, event, helper) {
        var filterFields = [];
        var filters = component.get("v.filters");
        for(var filter in filters) {
            filterFields.push(filters[filter].fieldName);
        }
        return filterFields;
    },
    
    buildWhereClause : function(component, event, helper) {
        var search_text = component.get("v.search_text");
        var whereClause = "";
        
        //Grabbing dynamic filter choices
        var filters = component.find("contentFilter");
        //For each filter
        for(var filter in filters) {
            //If you're seeing an error around here, check API version.
            //https://success.salesforce.com/issues_view?id=a1p3A000000mCJRQA2&title=lightning-component-works-on-safari-when-using-api-versions-40-but-breaks-with-versions-40
            var values = filters[filter].get("v.values");
            var fieldName = filters[filter].get("v.fieldName");
            
            //Only add to the whereclause if the filter has values to filter on
            if(values !== null && values !== undefined && values.length > 0) {
                if(whereClause !== "") { whereClause += " AND "; }
                whereClause += "(";
                
                //Each 'row' of this filter's addition to the query's where clause
                for(var value in values) {
                    whereClause += fieldName + " = '" + values[value] + "'";
                    if(value != values.length-1) { whereClause += " OR "; }
                }
                
                whereClause += ")";
            }
        }
        
        //Hardcoded for the Apollo App - For configurable, change to list of fields to compare search text against
        if(whereClause !== "") { whereClause += " AND "; }
        whereClause += "(Title LIKE '%" + search_text + "%'" + 
                       " OR " + "Description LIKE '%" + search_text + "%'" +
                       " OR " + "Description_Text__c LIKE '%" + search_text + "%'" +
                       " OR " + "CarestreamDSA__Mobile_App_Vanity_Name__c LIKE '%" + search_text + "%')";
        var isLatest = component.get("v.isLatest");
        if(isLatest === true) { whereClause += " AND isLatest = " + isLatest; }
        
        
        return whereClause;
    },
    
    saveFilters : function(component, event, helper) {
        //Adding these two since they are necessary in the persistent filter configuration,
        //but are not their own separate component
        var appFilters = {
            "filters": [{
                    fieldName : "fontSize",
                	values : component.get("v.fontSize")
                },
                {
                    fieldName : "isLatest",
                	values : component.get("v.isLatest")
                }
            ]
        }
        
        //Loop through the ContentFilter components and add their values to the Persistent filters list
        var filters = component.find("contentFilter");
        for(var filter in filters) {
            var fieldName = filters[filter].get("v.fieldName");
            var values = filters[filter].get("v.values");
            
            //if(fieldName != 'Apollo_Category__c') {
                var item = { 
                    "fieldName" : fieldName,
                    "values" : values
                };
                
                appFilters['filters'].push(item);
            //}
            
        }
        
        //Send off the bundled up JSON to be stored on the user
        this.remoteCall(component, 'saveAppFilters', {
            filters : JSON.stringify(appFilters)
        }, false, function(data) {});
    },
    
    noCategorySelected : function(component, event, helper) {
        var filters = component.find("contentFilter");
        
        //If there was no category filter selected, return a flag signalling that.
        for(var f in filters) {
            var fieldName = filters[f].get("v.fieldName");
            var values = filters[f].get("v.values");
            if(fieldName === 'Apollo_Category__c') {
                if(values === null || values === undefined 
                   || values.length === 0 || values[0] === null) {
                    return true; 
                }
                else {
                    return false;
                }
            }
        }
    }
        
        
        
        
        
        
})